package com.xiyang.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * 注意不可用lombok注解  他会重写hashcode和equals
 * @author xiyang.ycj
 * @since Dec 30, 2019 11:56:34 AM
 */
@Entity
@Table(name = "dev_role")
@org.hibernate.annotations.Table(appliesTo = "dev_role",comment = "角色表")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;
    @Column(name = "role_code",columnDefinition = "varchar(120) comment '角色代码' ")
    private String  roleCode;
    @Column(name = "role_name",columnDefinition = "varchar(120) comment '角色名称' ")
    private String  roleName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dev_role_permission", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = {
            @JoinColumn(name = "permission_id") })
    @JsonIgnore
    private Set<Permission> permissions;


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", roleCode='" + roleCode + '\'' +
                ", roleName='" + roleName + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
