package com.xiyang.common.service;

import com.xiyang.common.entity.User;
import org.springframework.stereotype.Service;

/**
 * @author xiyang.ycj
 * @since Dec 30, 2019 15:11:28 PM
 */
public interface UserService {
    /**
     * 根据用户账户查询用户信息
     * @param account 账户
     * @return user
     */
    User findUserByAccount(String account);

    /**
     * 修改密码
     * @param id
     * @param password
     * @return
     */
   Integer changePassword(Integer id,String password);

    /**
     * 保存用户
     * @param user
     * @return
     */
   User save(User user);
}
