package com.xiyang.common.repository;

import com.xiyang.common.entity.User;
import com.xiyang.common.entity.UserConfigure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author chuanjieyang
 * @since May 18, 2019 15:42:05 PM
 */
@Repository
public interface UserConfigureRepository extends JpaRepository<UserConfigure,Integer> , JpaSpecificationExecutor<UserConfigure> {
    UserConfigure findByUser(User user);
}
