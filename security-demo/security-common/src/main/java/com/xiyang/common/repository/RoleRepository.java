package com.xiyang.common.repository;

import com.xiyang.common.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xiyang.ycj
 * @since Jul 05, 2019 10:34:49 AM
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
    Role findByRoleCode(String rolename);
}
