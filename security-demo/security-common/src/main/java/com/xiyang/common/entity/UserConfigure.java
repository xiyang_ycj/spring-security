package com.xiyang.common.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author xiyang.ycj
 * @since Sep 10, 2019 15:54:02 PM
 */
@Data
@ToString
@Entity
@Table(name = "dev_user_configure")
@org.hibernate.annotations.Table(appliesTo = "dev_user_configure",comment = "用户配置表")
public class UserConfigure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_configure_id")
    private Integer id;

    @Column(name="proxy_ip_url", columnDefinition = "varchar(255) default null comment'返回路径'")
    private String proxyIpUrl;

    @Column(name="return_url", columnDefinition = "varchar(255) default null comment'回路径'")
    private String returnUrl;

    @Column(name="notify_url", columnDefinition = "varchar(255) default null comment'路径'")
    private String notifyUrl;

    @Column(name = "enabled_proxy", columnDefinition = "bit(1) NOT NULL DEFAULT b'1' comment '用户是否禁用代理ip true:1 false:0 默认true（禁用）' ")
    private Boolean enabledProxy;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "none")) /* 不生成外键 */
    private User user;

}
