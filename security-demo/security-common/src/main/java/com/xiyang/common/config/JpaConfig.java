package com.xiyang.common.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author xiyang.ycj
 * @since Jul 21, 2019 00:17:44 AM
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.xiyang.common.repository"})
@EntityScan(basePackages = {"com.xiyang.common.entity"})
public class JpaConfig {
}
