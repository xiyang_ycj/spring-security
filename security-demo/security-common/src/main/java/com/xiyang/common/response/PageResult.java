package com.xiyang.common.response;

import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class PageResult<T> implements Serializable, Iterable<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -164899598296679707L;

	private long total;

	private long page;

	private List<T> rows;

	private List<?> footer;

	private Map<String,?> other;

	public PageResult() {

	}

	public PageResult(long total, List<T> rows) {
		this.total = total;
		this.rows = rows;
	}

	public PageResult(long total, List<T> rows, List<?> footer) {
		this.total = total;
		this.rows = rows;
		this.footer = footer;
	}

	public PageResult(long total, List<T> rows, Map<String,?> other) {
		this.total = total;
		this.rows = rows;
		this.other = other;
	}

	public PageResult(long total, List<T> rows, List<?> footer,Map<String,?> other) {
		this.total = total;
		this.rows = rows;
		this.footer = footer;
		this.other = other;
	}

	public PageResult(Page<T> page) {
		this.total = page.getTotalElements();
		this.rows = page.getContent();
		this.page = page.getTotalPages();
	}

	public long getTotal() {
		return total;
	}

	public List<T> getRows() {
		return rows;
	}

	public long getPage() {
		return page;
	}

	public List<?> getFooter() {
		return footer;
	}

	public Map<String, ?> getOther() {
		return other;
	}

	public void setOther(Map<String, ?> other) {
		this.other = other;
	}

	@Override
	public Iterator<T> iterator() {
		return rows == null ? null : rows.iterator();
	}

}
