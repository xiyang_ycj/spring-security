package com.xiyang.common.service.impl;

import com.xiyang.common.entity.User;
import com.xiyang.common.repository.UserRepository;
import com.xiyang.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author xiyang.ycj
 * @since Dec 30, 2019 15:12:16 PM
 */
@Service
public class UserServiceImpl implements UserService {


    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User findUserByAccount(String account) {
        Optional<User> optional = repository.findUserByAccount(account);
        return optional.orElse(null);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer changePassword(Integer id, String password) {
        return repository.updatePassword(password,id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public User save(User user) {
        return repository.saveAndFlush(user);
    }
}
