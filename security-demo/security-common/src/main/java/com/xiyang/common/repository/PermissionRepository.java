package com.xiyang.common.repository;

import com.xiyang.common.entity.Permission;
import com.xiyang.common.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author xiyang.ycj
 * @since Jul 05, 2019 14:33:51 PM
 */
@Repository
public interface PermissionRepository extends JpaRepository<Permission,Integer> {
    List<Permission> findAllByRoles(Set<Role> roles);
    Set<Permission> findAllByIdIn(List<Integer> permissionId);
}
