package com.xiyang.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * 注意不可用lombok注解  他会重写hashcode和equals
 * @author xiyang.ycj
 * @since Dec 30, 2019 15:55:21 PM
 */
@Entity
@Table(name = "dev_permission")
@org.hibernate.annotations.Table(appliesTo = "dev_permission",comment = "角色表")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "permission_name",columnDefinition = "varchar(120) comment '权限名称' ")
    private String  permissionName;
    @Column(name = "description",columnDefinition = "varchar(120) comment '权限描述' ")
    private String  description;
    @Column(name = "url",columnDefinition = "varchar(120) comment '权限地址' ")
    private String  url;
    @Column(name = "code",columnDefinition = "varchar(120) comment '权限代码' ")
    private String  code;
    @Column(name = "type",columnDefinition = "tinyint(1) NOT NULL DEFAULT b'0' comment '是否是菜单,默认不是 0:路径，1，子菜单，2一级菜单' ")
    private Integer  type;
    @Column(name = "parent_id",columnDefinition = "int(11) comment '上级菜单ID' ")
    private Integer  parentId;
    @Column(name = "icon",columnDefinition = "varchar(120) comment '图标' ")
    private String  icon;
    @Column(name = "sort",columnDefinition = "bigint(20) comment '排序' ")
    private Long  sort;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dev_role_permission", joinColumns = { @JoinColumn(name = "permission_id") }, inverseJoinColumns = {
            @JoinColumn(name = "role_id") })
    @JsonIgnore
    private Set<Role> roles;

    @Transient
    @JsonIgnore
    private Set<String> roleCodes;

    public Set<String> getRoleCodes() {
        if(roleCodes==null){
            roleCodes=new HashSet<>();
            for (Role role : roles) {
                this.roleCodes.add(role.getRoleCode());
            }
        }
        return roleCodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(permissionName, that.permissionName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(url, that.url) &&
                Objects.equals(code, that.code) &&
                Objects.equals(type, that.type) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(icon, that.icon) &&
                Objects.equals(sort, that.sort) &&
                Objects.equals(roles, that.roles) &&
                Objects.equals(roleCodes, that.roleCodes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, permissionName, description, url, code, type, parentId, icon, sort, roles, roleCodes);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

}
