package com.xiyang.common.repository;

import com.xiyang.common.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 注意jpa2.0之后
 * findOne：返回实体的optional对象
 * getOne：返回实体的引用，代理对象
 * @author xiyang.ycj
 * @since Jun 22, 2019 01:28:22 AM
 */
@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    /**
     * 根据用户账户查询用户信息
     * @param account 账户
     * @return user
     */
    Optional<User> findUserByAccount(String account);

    /**
     * 修改密码
     * @param password
     * @param userId
     * @return
     */
    @Modifying
    @Query("update User set password = ?1 where userId =?2")
    Integer updatePassword(String password,Integer userId);

}
