package com.xiyang.common.response;

import java.io.Serializable;

/**
 * 返回格式
 * @author chuanjieyang
 * @since May 05, 2019 16:17:00 PM
 */
public final class BaseResult implements Serializable {
    private static final long serialVersionUID = 4241583739802575733L;

    /** 返回状态码 */
    private Integer code;

    /** 返回信息 */
    private String message;

    /** 返回数据 */
    private Object data;

    public BaseResult() {
    }

    public BaseResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResult(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static BaseResult fail(String message) {
        BaseResult result = new BaseResult();
        result.code = ReturnCode.FAILED.getCode();
        result.message = message;
        return result;
    }

    public static BaseResult fail(String message,Object data) {
        BaseResult result = new BaseResult();
        result.code = ReturnCode.FAILED.getCode();
        result.message = message;
        result.data = data;
        return result;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
