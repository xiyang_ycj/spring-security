/*
Navicat MySQL Data Transfer

Source Server         : xy
Source Server Version : 50725
Source Host           : rm-wz94036rfeyb20641do.mysql.rds.aliyuncs.com:3306
Source Database       : security

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-12-30 19:14:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dev_permission
-- ----------------------------
DROP TABLE IF EXISTS `dev_permission`;
CREATE TABLE `dev_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限代码',
  `description` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限描述',
  `icon` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级菜单ID',
  `permission_name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限名称',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是菜单,默认不是 0:路径，1，子菜单，2菜单',
  `url` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of dev_permission
-- ----------------------------
INSERT INTO `dev_permission` VALUES ('1', 'dev_buyer', '1管理', 'layui-icon layui-icon-extend layui-icon-buyer', null, '1管理', '1', '2', '/dev/buyer');
INSERT INTO `dev_permission` VALUES ('2', 'dev_goods', '2管理', 'layui-icon layui-icon-extend layui-icon-goods', null, '2管理', '2', '2', '/dev/goods');
INSERT INTO `dev_permission` VALUES ('3', 'dev_store', '3管理', 'layui-icon layui-icon-extend layui-icon-store', null, '3管理', '3', '2', '/dev/store');
INSERT INTO `dev_permission` VALUES ('4', 'dev_order', '4管理', 'layui-icon layui-icon-extend layui-icon-order', null, '4管理', '4', '2', '/dev/order');
INSERT INTO `dev_permission` VALUES ('5', 'log', '5管理', 'layui-icon layui-icon-log', null, '5管理', '5', '3', 'log');
INSERT INTO `dev_permission` VALUES ('6', 'dev_callback', '5管理-1', 'layui-icon layui-icon-log', '5', '5管理-1', '6', '1', '/dev/callback');
INSERT INTO `dev_permission` VALUES ('7', 'code_stock_manage', '6管理', 'layui-icon layui-icon-template-1', null, '6管理', '13', '3', '/codeStock');
INSERT INTO `dev_permission` VALUES ('8', 'code_stock_pre_order', '6管理-1', null, '7', '6管理-1', '14', '1', '/codeStock/preOrder');
INSERT INTO `dev_permission` VALUES ('9', 'code_stock_info', '6管理-2', null, '7', '6管理-2', '15', '1', '/codeStock/index');
INSERT INTO `dev_permission` VALUES ('10', 'configure', '设置', 'layui-icon layui-icon-set', null, '设置', '19', '3', '/configure');
INSERT INTO `dev_permission` VALUES ('11', 'system_configure', '系统设置', null, '10', '系统设置', '20', '0', 'system/configure');
INSERT INTO `dev_permission` VALUES ('12', 'order_configure', '1配置', null, '11', '1配置', '10', '0', 'user/order/configure');
INSERT INTO `dev_permission` VALUES ('13', 'user_configure', '我的设置', null, '10', '我的设置', '21', '0', 'user/configure');
INSERT INTO `dev_permission` VALUES ('14', 'user_basicData', '基本资料', null, '13', '基本资料', '22', '0', 'user/basicData');
INSERT INTO `dev_permission` VALUES ('15', 'user_changePassword', '修改密码', null, '13', '修改密码', '23', '0', 'user/changePassword');
INSERT INTO `dev_permission` VALUES ('16', 'admin_manage', '平台管理', null, null, '平台账户管理', '16', '2', '/admin/index');
INSERT INTO `dev_permission` VALUES ('17', 'admin_register', '注册账号', null, null, '账号注册', '17', '2', '/admin/register');
INSERT INTO `dev_permission` VALUES ('18', 'admin_role', '角色注册', null, null, '角色注册', '18', '2', '/admin/role');

-- ----------------------------
-- Table structure for dev_role
-- ----------------------------
DROP TABLE IF EXISTS `dev_role`;
CREATE TABLE `dev_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色代码',
  `role_name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of dev_role
-- ----------------------------
INSERT INTO `dev_role` VALUES ('1', 'ROLE_ADMIN', '平台管理员');
INSERT INTO `dev_role` VALUES ('2', 'ROLE_TEST', '测试角色');

-- ----------------------------
-- Table structure for dev_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `dev_role_permission`;
CREATE TABLE `dev_role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `FK9tx50w25el74x981cuhd580py` (`role_id`),
  CONSTRAINT `FK9tx50w25el74x981cuhd580py` FOREIGN KEY (`role_id`) REFERENCES `dev_role` (`role_id`),
  CONSTRAINT `FKtdg2ky40s637kb0gfxcudaj74` FOREIGN KEY (`permission_id`) REFERENCES `dev_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of dev_role_permission
-- ----------------------------
INSERT INTO `dev_role_permission` VALUES ('1', '10');
INSERT INTO `dev_role_permission` VALUES ('1', '13');
INSERT INTO `dev_role_permission` VALUES ('1', '14');
INSERT INTO `dev_role_permission` VALUES ('1', '15');
INSERT INTO `dev_role_permission` VALUES ('1', '16');
INSERT INTO `dev_role_permission` VALUES ('1', '17');
INSERT INTO `dev_role_permission` VALUES ('1', '18');
INSERT INTO `dev_role_permission` VALUES ('2', '1');
INSERT INTO `dev_role_permission` VALUES ('2', '2');
INSERT INTO `dev_role_permission` VALUES ('2', '3');
INSERT INTO `dev_role_permission` VALUES ('2', '4');
INSERT INTO `dev_role_permission` VALUES ('2', '5');
INSERT INTO `dev_role_permission` VALUES ('2', '6');
INSERT INTO `dev_role_permission` VALUES ('2', '7');
INSERT INTO `dev_role_permission` VALUES ('2', '8');
INSERT INTO `dev_role_permission` VALUES ('2', '9');
INSERT INTO `dev_role_permission` VALUES ('2', '10');
INSERT INTO `dev_role_permission` VALUES ('2', '11');
INSERT INTO `dev_role_permission` VALUES ('2', '12');
INSERT INTO `dev_role_permission` VALUES ('2', '13');
INSERT INTO `dev_role_permission` VALUES ('2', '14');
INSERT INTO `dev_role_permission` VALUES ('2', '15');

-- ----------------------------
-- Table structure for dev_user
-- ----------------------------
DROP TABLE IF EXISTS `dev_user`;
CREATE TABLE `dev_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '账号',
  `nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登陆密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of dev_user
-- ----------------------------
INSERT INTO `dev_user` VALUES ('1', 'admin', '管理员', '$2a$04$La3qzeBIHzKMQube1eMnFOP3ky8HaOrVH/3.4EuS786rEo1OCl3Ay');
INSERT INTO `dev_user` VALUES ('2', '123', '123', '$2a$04$MmI/pJT7rXmIrm2g447x8OwXsRBv7PhQA1W3pUXcYsmVsVnoFwFiG');
INSERT INTO `dev_user` VALUES ('3', '1234', '1234', '$2a$04$gnp9jyygLjI/5AXard9l5Oh82YgwyWZPR/9doHN1I64Zr9R5rz9za');

-- ----------------------------
-- Table structure for dev_user_configure
-- ----------------------------
DROP TABLE IF EXISTS `dev_user_configure`;
CREATE TABLE `dev_user_configure` (
  `user_configure_id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled_proxy` bit(1) NOT NULL DEFAULT b'1' COMMENT '用户是否禁用代理ip true:1 false:0 默认true（禁用）',
  `notify_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '路径',
  `proxy_ip_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '返回路径',
  `return_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '回路径',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_configure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户配置表';

-- ----------------------------
-- Records of dev_user_configure
-- ----------------------------

-- ----------------------------
-- Table structure for dev_user_role
-- ----------------------------
DROP TABLE IF EXISTS `dev_user_role`;
CREATE TABLE `dev_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKri1jpdho1da9roqq58l5jfg6s` (`role_id`),
  CONSTRAINT `FKkisb73h8ujsjxuvpivyy2qls6` FOREIGN KEY (`user_id`) REFERENCES `dev_user` (`id`),
  CONSTRAINT `FKri1jpdho1da9roqq58l5jfg6s` FOREIGN KEY (`role_id`) REFERENCES `dev_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of dev_user_role
-- ----------------------------
INSERT INTO `dev_user_role` VALUES ('1', '1');
INSERT INTO `dev_user_role` VALUES ('2', '1');
INSERT INTO `dev_user_role` VALUES ('3', '2');
