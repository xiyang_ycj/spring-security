package com.xiyang.web.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author xiyang.ycj
 * @since Sep 10, 2019 15:54:02 PM
 */

@Data
@ToString
public class UserConfigureVO {

    private Integer id;

    private String notifyUrl;

    private String proxyIpUrl;

    private String returnUrl;

    private String proxy="";

}
