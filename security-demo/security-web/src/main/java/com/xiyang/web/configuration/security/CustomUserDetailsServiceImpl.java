package com.xiyang.web.configuration.security;

import com.xiyang.common.entity.User;
import com.xiyang.common.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * 核心组件值之UserDetailsService
 * 用来加载用户信息
 *
 * 默认实现
 * UserDetailsService => UserDetailsManager => InMemoryUserDetailsManager => 存储于内存
 * UserDetailsService => JdbcDaoImpl => 存储于数据库（磁盘）
 *
 * 其中，如果你的数据库设计符合JdbcDaoImpl中的规范，你也就不用自己去实现UserDetailsService了。
 * 但是大多数情况是不符合的，因为你用户表不一定就叫users，可能还有其他前缀什么的，比如叫tb_users。
 * 或者字段名也跟它不一样。如果你一定要使用这个JdbcDaoImpl，你可以通过它的setter方法修改它的数据库查询语句。
 * 注：它是利用Spring框架的JdbcTemplate来查询数据库的
 *
 * UserDetailsService最终注入到AuthenticationProvider的实现类中。
 * 注意：UserDetailsService只单纯的负责存取用户信息，没有其他功能；认证过程是由AuthenticationManager完成
 * @author xiyang.ycj
 * @since Jun 24, 2019 23:48:14 PM
 */
@Component
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsServiceImpl.class);

    private final UserService userService;

    @Autowired
    public CustomUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        LOGGER.info("对用户 [{}] 进行信息加载...",account);
        User user = userService.findUserByAccount(account);
        if(user==null){
            LOGGER.error("用户 [{}] 未找到",account);
            throw new UsernameNotFoundException("Username：["+account+"] not found");
        }
        LOGGER.info("用户 [{}] 信息加载完成",account);
        // SecurityUser 实现UserDetails
        return new SecurityUser(user);
    }
}
