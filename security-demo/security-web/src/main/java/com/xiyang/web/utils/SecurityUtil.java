package com.xiyang.web.utils;

import com.xiyang.web.configuration.security.SecurityUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author xiyang.ycj
 * @since Jul 17, 2019 17:34:43 PM
 */
public class SecurityUtil {

    /**
     * 当前登录用户的信息
     * @return 返回当前登录用户的信息
     */
    public static SecurityUser getSecurityUser(){
        return ((SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
