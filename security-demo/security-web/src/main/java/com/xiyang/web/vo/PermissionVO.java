package com.xiyang.web.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author xiyang.ycj
 * @since Sep 12, 2019 11:40:43 AM
 */
@Data
@ToString
public class PermissionVO {


    private Integer id;

    private String  permissionName;

    private String  description;

    private String  url;

    private String  code;

    private Integer  type;

    private Integer  parentId;

    private List<PermissionVO> child;

    private String  icon;

    private Long  sort;
}
