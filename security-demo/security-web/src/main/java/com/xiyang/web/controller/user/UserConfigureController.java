package com.xiyang.web.controller.user;

import com.xiyang.common.response.BaseResult;
import com.xiyang.common.response.ReturnCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author xiyang.ycj
 * @since Sep 24, 2019 16:34:23 PM
 */
@Controller
@RequestMapping("/userConfig")
public class UserConfigureController {


    @GetMapping(value = "/checkProxy")
    @ResponseBody
    public BaseResult checkProxy() {

        return new BaseResult(ReturnCode.SUCCESS.getCode(), ReturnCode.SUCCESS.getMessage());
    }
}
