package com.xiyang.web.vo;

import lombok.Data;

import java.util.Set;

/**
 * @author chuanjieyang
 * @since May 20, 2019 15:17:23 PM
 */
@Data
public class UserVO {
    private Integer userId;
    private String account;
    private String password;
    private String nickname;
    private Integer sex;
    private String role;
    private Set<String> roleNames;


}
