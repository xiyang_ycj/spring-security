package com.xiyang.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author xiyang.ycj
 * @since Dec 26, 2019 18:46:02 PM
 */
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = {"com.xiyang.common","com.xiyang.web"})
public class SecurityWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityWebApplication.class, args);
    }

}
