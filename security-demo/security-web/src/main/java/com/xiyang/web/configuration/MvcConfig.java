package com.xiyang.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author xiyang.ycj
 * @since Dec 30, 2019 11:38:49 AM
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/","/index");
        registry.addViewController("/logins").setViewName("login");
        registry.addViewController("/admin/role").setViewName("admin/role");
        registry.addViewController("/admin/editPermissionView").setViewName("admin/edit_permission");
        registry.addViewController("/admin/addRoleView").setViewName("admin/add_role");
    }
}
