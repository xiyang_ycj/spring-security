package com.xiyang.web.controller;

import com.xiyang.common.entity.Permission;
import com.xiyang.common.entity.Role;
import com.xiyang.common.response.BaseResult;
import com.xiyang.common.response.ReturnCode;
import com.xiyang.common.service.UserService;
import com.xiyang.web.utils.JsonUtil;
import com.xiyang.web.utils.SecurityUtil;
import com.xiyang.web.vo.PermissionVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author chuanjieyang
 * @since May 20, 2019 14:56:44 PM
 */
@Controller
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    UserService userService;

    @GetMapping("/login_page")
    @ResponseBody
    public Object loginPage() {
        return new BaseResult(ReturnCode.FAILED.getCode(), "尚未登录，请登录!");
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(ModelMap map) {
        Set<Permission> permissions  = new HashSet<>(0);
        Set<Role> roles = SecurityUtil.getSecurityUser().getRoles();
        roles.forEach(role -> permissions.addAll(role.getPermissions()));
        List<PermissionVO> permissionVOs = new ArrayList<>();
        /* 多级按钮 */
        for (Permission permission : permissions) {

            /* 二级 */
            List<PermissionVO> collect  = new ArrayList<>();
            permissions.stream().filter(item -> item.getParentId()!=null&&item.getParentId().equals(permission.getId())).sorted(Comparator.comparing(Permission::getSort)).collect(Collectors.toList()).forEach(item-> collect.add(JsonUtil.clone(item, PermissionVO.class)));
            PermissionVO clone = JsonUtil.clone(permission, PermissionVO.class);

            /* 三级 */
            for (PermissionVO permissionVO : collect) {
                List<PermissionVO> collectSecondLevel  = new ArrayList<>();
                permissions.stream().filter(item -> item.getParentId()!=null&&item.getParentId().equals(permissionVO.getId())).sorted(Comparator.comparing(Permission::getSort)).forEach(item-> collectSecondLevel.add(JsonUtil.clone(item, PermissionVO.class)));
                permissionVO.setChild(collectSecondLevel);
            }

            clone.setChild(collect);
            permissionVOs.add(clone);
        }
        map.put("username", SecurityUtil.getSecurityUser().getUsername());
        map.put("role", SecurityUtil.getSecurityUser().getRoles().iterator().next().getRoleCode());
        map.put("permissions",permissionVOs.stream().sorted(Comparator.comparing(PermissionVO::getSort)).collect(Collectors.toList()));
        return "index";
    }

}
