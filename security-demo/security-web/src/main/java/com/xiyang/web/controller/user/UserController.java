package com.xiyang.web.controller.user;

import com.xiyang.common.entity.User;
import com.xiyang.common.response.BaseResult;
import com.xiyang.common.response.ReturnCode;
import com.xiyang.common.service.UserService;
import com.xiyang.web.utils.SecurityUtil;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


/**
 * 用户控制器
 * @author xiyang.ycj
 * @since Sep 10, 2019 18:05:55 PM
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    @RequestMapping("/order/configure")
    public String orderConfigure(ModelMap map){
        map.put("user","");
        return "user/system/order_configure";
    }

    /* 基本资料页面 */
    @RequestMapping("/basicData")
    public String basicData(ModelMap map){
        map.put("user",SecurityUtil.getSecurityUser());
        return "user/personal/user_basicData";
    }

    /* 修改密码页面 */
    @RequestMapping("/changePassword")
    public String changePassword(ModelMap map){
        map.put("user",SecurityUtil.getSecurityUser());
        return "user/personal/user_changePassword";
    }

    @GetMapping("/checkPassword")
    @ResponseBody
    public BaseResult checkPassword(@RequestParam String password){
        try {
            boolean matches = bCryptPasswordEncoder.matches(password, SecurityUtil.getSecurityUser().getPassword());
            if(matches)
            return new BaseResult(ReturnCode.SUCCESS.getCode(),ReturnCode.SUCCESS.getMessage());
        } catch (Exception e){
            e.printStackTrace();
        }
        return new BaseResult(ReturnCode.FAILED.getCode(),ReturnCode.FAILED.getMessage());
    }


    @PostMapping(value = "/savePassword",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public BaseResult savePassword(@RequestParam("password") String password){
        try {
            User user = SecurityUtil.getSecurityUser();
            userService.changePassword(user.getId(),bCryptPasswordEncoder.encode(password));
            return new BaseResult(ReturnCode.SUCCESS.getCode(),ReturnCode.SUCCESS.getMessage());
        } catch (Exception e){
            e.printStackTrace();
        }
        return new BaseResult(ReturnCode.FAILED.getCode(),ReturnCode.FAILED.getMessage());
    }

    @PostMapping(value = "/saveUserInfo",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public BaseResult saveUserInfo(@RequestParam("nickname") String nickname){
        try {
            User user = SecurityUtil.getSecurityUser();
            user.setNickname(nickname);
            userService.save(user);
            return new BaseResult(ReturnCode.SUCCESS.getCode(),ReturnCode.SUCCESS.getMessage());
        } catch (Exception e){
            e.printStackTrace();
        }
        return new BaseResult(ReturnCode.FAILED.getCode(),ReturnCode.FAILED.getMessage());
    }

}
