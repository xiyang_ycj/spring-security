;layui.define("jquery", function (exports) {
    "use strict";

    let modelName = 'nav', /* 模块名 */
        $ = layui.$, /* 启用jquery */
        win = $(window), /* 获取窗口对象 */
        shrinkButton = '#LAY_app_shrinkButton', /* 扩展收缩图标 */
        sb = $(shrinkButton),
        /* 收缩 */
        sideShrink = 'layui-side-shrink',
        sideSpread = 'layui-side-spread',
        /* 图标 */
        spread = 'layui-icon-spread-left',
        shrink = 'layui-icon-shrink-right',

        nav = ({
            config: {"ss": 11},
            on: function (events, callback) {
                return layui.onevent.call(this, modelName, events, callback)
                /*
                    layui.event.call(elem,'form','select(reginChange)',data);
                    //elem,表示事件函数中的this，
                    //form,表示模块，固定值
                    /'select(reginChange),需要触发的filter
                    //callback,事件回调函数
                 */
                /*
                    call 改变this指向
                         第一个参数是this值没有变化，变化的是其余参数都直接传递给函数。在使用call（）方法时，传递给函数的参数必须逐个列举出来。

                         function add(c, d){
                            return this.a + this.b + c + d;
                         }
                         var o = {a:1, b:3};
                         add.call(o, 5, 7); // 1 + 3 + 5 + 7 = 16
                 */
            }
        }),
        implement = function (e) { /* 构造函数 模块具体实现 */
            let t = this;
            t.config = $.extend({}, t.config, nav.config, e), t.render();
            /*
                jQuery.extend( target, object1, [objectN])
                用一个或多个其他对象来扩展一个对象，返回被扩展的对象

                let result=$.extend({},{name:"Tom",age:21},{name:"Jerry",sex:"Boy"})

                result={name:"Jerry",age:21,sex:"Boy"}

                注意：后面的参数如果和前面的参数存在相同的名称，那么后面的会覆盖前面的参数值。
             */
            console.log(t.config);
        };
    implement.prototype.config = {
        width: "600px",
        height: "280px",
        full: !1,
        arrow: "hover",
        indicator: "inside",
        autoplay: !0,
        interval: 3e3,
        anim: "",
        trigger: "click",
        index: 0
    }, implement.prototype.render = function () {
        let e = this, config = e.config;
        /* this 指向的是 implement*/
        config.elem = $(config.elem);
        let filterName = config.elem.attr("lay-filter");
        console.log(config);
        console.log(filterName);
        func.iconSwitch();
        win.resize(function () {
            func.iconSwitch();
           // layui.table.resize("demo")
        });
    }, nav.render = function (e) { /* 建造实例 */
        return new implement(e);
    };
    let
        func = {
            screen: function () { /* 窗口大小 */
                let e = win.width();
                return e > 1200 ? 3 : e > 992 ? 2 : e > 768 ? 1 : 0
            },
            iconSwitch: function () { /* 图标切换 */
                let l = func.screen();
                l < 2? sb.removeClass(shrink).addClass(spread):
                    sb.addClass(shrink).removeClass(spread);
            },
            sideFlexible: () => { /* 导航收缩/展开和监听 */
                let app = $("#LAY_app"), sb = $(shrinkButton);
                let e = sb.hasClass(spread);
                /* 判断是否时扩展图标  true 需要收缩:false 需要展开 */
                let l = func.screen();
                console.log(l);
                e === !0 ? (sb.removeClass(spread).addClass(shrink), l < 2 ? app.addClass(sideSpread) : app.removeClass(sideSpread), app.removeClass(sideShrink))
                    : (sb.removeClass(shrink).addClass(spread), l < 2 ? app.removeClass(sideShrink) : app.addClass(sideShrink), app.removeClass(sideSpread)),
                    layui.event.call(this, modelName, "shrink(" + sb.attr('lay-filter') + ")", {status: e})
            }
        };

    let b = $(document);
    b.on("click", shrinkButton, func.sideFlexible);
    console.group("================nav模块=====================>")
    exports(modelName, nav)
});


/* layui.event.call(this, modelName, "change(ss)", {
          index: 1,
          prevIndex: 0,
          item: 1
      })*/