;layui.define(function (exports) {
    "use strict";
    let modelName = 'preOrder', /* 模块名 */
        $ = layui.$, /* 启用jquery */

        carousel = layui.carousel,
        nav = layui.nav,
        win = $(window), /* 获取窗口对象 */

        preOrder = ({
            config: {},
            on: function (events, callback) {
                return layui.onevent.call(this, modelName, events, callback);
            }
        }),
        implement = function (e) { /* 构造函数 模块具体实现 */
            let t = this;
            t.config = $.extend({}, t.config, preOrder.config, e), t.render();
        };
    implement.prototype.config = {},
        implement.prototype.render = function () {
            let e = this, config = e.config;
            /* this 指向的是 implement*/
            config.elem = $(config.elem);
            let pie = {
                title: {text: "", x: "center", textStyle: {fontSize: 14, fontWeight: 45}},
                tooltip: {trigger: "item", formatter: "{a} <br/>{b} : {c} 条 ({d}%)"},
                legend: {
                    orient: "vertical",
                    x: "left",
                    data: [],
                    icon: 'path://M857.499533 471.117665C776.574724 369.214358 651.751301 303.804427 511.494276 303.804427c-139.62487 0-264.041908 64.786807-345.00284 165.904436L374.867625 604.177158c40.846212-13.419737 84.473904-20.725637 129.817444-20.725637 51.060023 0 99.880413 9.310733 145.052368 26.189258l17.673234-11.785166-20.508898 10.620196 20.508898-10.620196',
                },
                color: [],
                series: [{
                    name: "",
                    type: "pie",
                    radius: "60%", /* 饼图的半径 */
                    center: ["50%", "50%"],
                    data: []
                }]
            };

            let payTypePie = Object.assign({}, pie);
            let payStatePie = Object.assign({}, pie);

            let xAxis = {
                /* 坐标轴轴线 */
                axisLine: {show: !0, lineStyle: {color: "#009688", width: 2}},
                /* 坐标轴刻度线 */
                axisTick: {show: !1},
                /* 分隔线 */
                splitLine: {show: !0, lineStyle: {color: ["#ccc"], width: 1, type: "solid"}},
            };

            let yAxis = Object.assign({}, xAxis);

            xAxis.type = "value";
            /* 分隔区域 */
            xAxis.splitArea = {show: !0, areaStyle: {color: ["rgba(200,200,200,0.1)", "rgba(250,250,250,0.3)"]}};
            /* 刻度标签值（坐标下的数值） */
            xAxis.axisLabel = {show: !0};

            yAxis.type = "category";
            yAxis.data = [];
            yAxis.splitArea = {show: !1};
            yAxis.axisLabel = {show: !0, interval: "auto", rotate: 0, margin: 8, textStyle: {color: "#333"}};

            let series = {
                name: "",
                type: "bar",
                stack: "总量",
                itemStyle: {
                    normal: {
                        label: {show: !0, position: "insideRight"},
                        barBorderRadius: 2,
                        emphasis: {barBorderRadius: 5}
                    }
                },
                data: []
            };


            let chartsContainers = $("#LAY-index-dataview").children("div"),
                l = [],
                options = [
                    payTypePie,
                    {
                        color: [],
                        tooltip: {trigger: "axis", axisPointer: {type: "shadow"}},
                        legend: {data: []},
                        xAxis: [xAxis],
                        yAxis: [yAxis],
                        series: [series, Object.assign({}, series)]
                    }
                ];
            if (chartsContainers[0]) {
                $.get("/codeStock/payTypeData", function (res) {
                    let legendData = [];
                    let seriesData = [];
                    for (const item of res.data.pie) {
                        let payType;
                        switch (item.payType) {
                            case "WE_CHAT_WAP" :
                                payType = "微信wap";
                                break;
                            case "ALI_PAY_WAP" :
                                payType = "支付宝wap";
                                break;
                        }
                        legendData.push(payType);
                        /* 图例的数据数组 */
                        const obj = {
                            value: item.count,
                            name: payType,
                        };
                        seriesData.push(obj);
                        /* 系列列表数据数组 */
                    }
                    options[0].title.text = "支付类型分布";
                    options[0].series[0].name = "支付类型";
                    options[0].legend.data = legendData;
                    options[0].series[0].data = seriesData;
                    if (options[0].legend.data[0] === "支付宝wap") {
                        options[0].color = ["#1E9FFF", "#5FB878"];
                        /* 调色盘颜色列表 */
                    } else options[0].color = ["#5FB878", "#1E9FFF"];

                    /*********************************/

                    for (const item of res.data.bar) {
                        let payType;
                        switch (item.payType) {
                            case "WE_CHAT_WAP" :
                                payType = "微信wap";
                                break;
                            case "ALI_PAY_WAP" :
                                payType = "支付宝wap";
                                break;
                        }
                        options[1].legend.data.push(payType);
                        /* 图例的数据数组 */
                        options[1].yAxis[0].data.push((item.money / 100).toFixed(2));
                        /* y轴数组 */
                    }

                    /* 去重 */
                    options[1].legend.data = common.uniqueOfArray(options[1].legend.data);
                    options[1].yAxis[0].data = common.uniqueOfArray(options[1].yAxis[0].data);

                    /* 系列名称 */
                    options[1].series[0].name = options[1].legend.data[0];
                    options[1].series[1].name = options[1].legend.data[1];

                    let ali = [];
                    let we = [];
                    for (const yAxisDatum of options[1].yAxis[0].data) {
                        let temp = [null, null];
                        res.data.bar.forEach((item, index, arr) => {
                            if (yAxisDatum === (item.money / 100).toFixed(2)) {
                                switch (item.payType) {
                                    case "WE_CHAT_WAP":
                                        temp[1] = item.count;
                                        break;
                                    case "ALI_PAY_WAP":
                                        temp[0] = item.count;
                                        break;
                                }
                            }
                        });
                        ali.push(temp[0]);
                        /* 系列一 数据 */
                        we.push(temp[1]);
                        /* 系列二 数据 */
                    }

                    if (options[1].legend.data[0] === "支付宝wap") {
                        options[1].color.push("#1E9FFF", "#5FB878");
                    } else options[1].color.push("#5FB878", "#1E9FFF");

                    options[1].series[0].data = ali;
                    options[1].series[1].data = we;

                    func.drawDataview(0, l, chartsContainers, options);
                });

                func.switchCharts("LAY-index-dataview", l, chartsContainers, options);
            }

            let payStateChartsContainers = $("#LAY-index-dataview-payState").children("div"),
                payStateChartsObjArr = [],
                payStateOptions = [
                    {
                        color: [],
                        tooltip: {trigger: "axis", axisPointer: {type: "shadow"}},
                        legend: {data: []},
                        xAxis: [xAxis],
                        yAxis: [Object.assign({}, yAxis)],
                        series: [{
                            name: "",
                            type: "bar",
                            stack: "总量", // 树状图叠加
                            itemStyle: {
                                normal: {
                                    label: {show: !0, position: "insideRight"},
                                    barBorderRadius: 2,
                                    emphasis: {barBorderRadius: 5}
                                }
                            },
                            data: []
                        }, {
                            name: "",
                            type: "bar",
                            stack: "总量",
                            itemStyle: {
                                normal: {
                                    label: {show: !0, position: "insideRight"},
                                    barBorderRadius: 2,
                                    emphasis: {barBorderRadius: 5}
                                }
                            },
                            data: []
                        }, {
                            name: "",
                            type: "bar",
                            stack: "总量",
                            itemStyle: {
                                normal: {
                                    label: {show: !0, position: "insideRight"},
                                    barBorderRadius: 2,
                                    emphasis: {barBorderRadius: 5}
                                }
                            },
                            data: []
                        }]
                    }, {
                        title: {text: "", x: "center", textStyle: {fontSize: 14, fontWeight: 45}},
                        tooltip: {trigger: "item", formatter: "{a} <br/>{b} : {c} 条 ({d}%)"},
                        legend: {
                            orient: "vertical",
                            x: "left",
                            data: [],
                            icon: 'path://M857.499533 471.117665C776.574724 369.214358 651.751301 303.804427 511.494276 303.804427c-139.62487 0-264.041908 64.786807-345.00284 165.904436L374.867625 604.177158c40.846212-13.419737 84.473904-20.725637 129.817444-20.725637 51.060023 0 99.880413 9.310733 145.052368 26.189258l17.673234-11.785166-20.508898 10.620196 20.508898-10.620196',
                        },
                        color: [],
                        series: [{
                            name: "",
                            type: "pie",
                            radius: "60%", /* 饼图的半径 */
                            center: ["50%", "50%"],
                            data: []
                        }]
                    }];

            if (payStateChartsContainers[0]) {
                $.get("/codeStock/stateData", function (res) {
                    let legendData = [];
                    let seriesData = [];
                    for (const item of res.data.pie) {
                        let payType;
                        switch (item.state) {
                            case 0 :
                                payType = "未支付";
                                break;
                            case 1 :
                                payType = "已支付";
                                break;
                            case 2 :
                                payType = "已失效";
                                break;
                        }
                        legendData.push(payType);
                        const obj = {
                            value: item.count,
                            name: payType,
                        };
                        seriesData.push(obj);
                    }
                    payStateOptions[1].title.text = "支付状态统计";
                    payStateOptions[1].series[0].name = "支付状态";
                    payStateOptions[1].legend.data = legendData;
                    payStateOptions[1].series[0].data = seriesData;
                    if (payStateOptions[1].legend.data[0] === "未支付") {
                        payStateOptions[1].color = ["#FFB800", "#5FB878", "#FF5722"];
                        /* 调色盘颜色列表 */
                    } else payStateOptions[1].color = ["#FFB800", "#5FB878", "#FF5722"];

                    /*********************************/

                    let legendDataOfBar = [];
                    let yAxisData = [];

                    for (const item of res.data.bar) {
                        let payType;
                        switch (item.state) {
                            case 0 :
                                payType = "未支付";
                                break;
                            case 1 :
                                payType = "已支付";
                                break;
                            case 2 :
                                payType = "已失效";
                                break;
                        }
                        legendDataOfBar.push(payType);
                        yAxisData.push((item.money / 100).toFixed(2));
                    }

                    legendDataOfBar = common.uniqueOfArray(legendDataOfBar);

                    yAxisData = common.uniqueOfArray(yAxisData);

                    let no = [];
                    let yes = [];
                    let expire = [];
                    for (const yAxisDatum of yAxisData) {
                        let temp = [null, null, null];
                        res.data.bar.forEach((item, index, arr) => {
                            if (yAxisDatum === (item.money / 100).toFixed(2)) {
                                switch (item.state) {
                                    case 0:
                                        temp[0] = item.count;
                                        break;
                                    case 1:
                                        temp[1] = item.count;
                                        break;
                                    case 2:
                                        temp[2] = item.count;
                                        break;
                                }
                            }
                        });
                        no.push(temp[0]);
                        yes.push(temp[1]);
                        expire.push(temp[2]);
                    }
                    // payStateOptions[0].legend.data = legendDataOfBar;
                    payStateOptions[0].legend.data = ["未支付", "已支付", "已失效"];
                    payStateOptions[0].yAxis[0].data = yAxisData;
                    payStateOptions[0].series[0].name = "未支付";
                    payStateOptions[0].series[1].name = "已支付";
                    payStateOptions[0].series[2].name = "已失效";
                    payStateOptions[0].series[0].data = no;
                    payStateOptions[0].series[1].data = yes;
                    payStateOptions[0].series[2].data = expire;
                    if (payStateOptions[0].legend.data[0] === "未支付") {
                        payStateOptions[0].color = ["#FFB800", "#5FB878", "#FF5722"];
                        /* 调色盘颜色列表 */
                    } else payStateOptions[0].color = ["#FFB800", "#5FB878", "#FF5722"];

                    func.drawDataview(0, payStateChartsObjArr, payStateChartsContainers, payStateOptions);
                });

                func.switchCharts("LAY-index-dataview-payState", payStateChartsObjArr, payStateChartsContainers, payStateOptions);
            }


            let tradeChartsContainers = $("#LAY-index-dataview-trade").children("div"),

                tradeChartsObjArr = [],
                tradeOptions = [
                    {

                        title: {
                            subtext: '金额统计（单位:元）',
                        },
                        legend: {data: ['下单金额','交易金额']},
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        color: [],
                        tooltip: {trigger: "axis", axisPointer: {type: "shadow"}, formatter: '{b}<br />{a0}: {c0}元<br />{a1}: {c1}元'},
                        xAxis: [Object.assign({}, yAxis)],
                        yAxis: [xAxis],
                        series: [
                            {
                                name: "",
                                type: "bar",
                                barWidth: 25,
                                barGap: '-100%', // Make series be overlap
                                markLine : {
                                    lineStyle: {
                                        normal: {
                                            type: 'dashed'
                                        }
                                    },
                                    data : [
                                        [{type : 'min'}, {type : 'max'}]
                                    ]
                                },
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: !0, position: "top", formatter: function (params) { // 数值为0时，不显示数值0
                                                if (params.value > 0) {
                                                    return toThousands(params.value)+"元";
                                                } else {
                                                    return ' ';
                                                }
                                            },
                                        },
                                        barBorderRadius: 2,
                                        emphasis: {barBorderRadius: 5}
                                    }
                                }, data: []
                            },
                            {
                                name: "",
                                type: "bar",
                                barWidth: 25,
                                markLine : {
                                    lineStyle: {
                                        normal: {
                                            type: 'dashed'
                                        }
                                    },
                                    data : [
                                        [{type : 'min'}, {type : 'max'}]
                                    ]
                                },
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: !0, position: "top", formatter: function (params) { // 数值为0时，不显示数值0
                                                if (params.value > 0) {
                                                    return toThousands(params.value)+"元";
                                                } else {
                                                    return ' ';
                                                }
                                            },
                                        },
                                        barBorderRadius: 2,
                                        emphasis: {barBorderRadius: 5}
                                    }
                                }, data: []
                            }
                        ]
                    }];

            if (tradeChartsContainers[0]) {
                $.get("/order/tradeOfWeek/20", function (res) {

                    $("#orderOfToday").html(toThousands(res.data.orderOfToday / 100)+'元');
                    $("#tradeOfToday").html(toThousands(res.data.tradeOfToday / 100)+'元');
                    $("#order").html(toThousands(res.data.order / 100)+'元');
                    $("#trade").html(toThousands(res.data.trade / 100)+'元');


                    let seriesData = [[],[]];
                    let xAxisData = [];

                    for (const item of res.data.orderOfDay) {
                        xAxisData.push(dateFormat(item.date, "MM-dd"));
                        seriesData[0].push((item.sumMoney / 100));
                    }
                    for (const item of res.data.tradeOfDay) {
                        seriesData[1].push((item.sumMoney / 100));
                    }

                    tradeOptions[0].xAxis[0].data = xAxisData;
                    tradeOptions[0].series[0].name = "下单金额";
                    tradeOptions[0].series[1].name = "交易金额";
                    tradeOptions[0].series[0].data = seriesData[0];
                    tradeOptions[0].series[1].data = seriesData[1];
                    tradeOptions[0].color = ["#1E9FFF","#5FB878"];
                    //tradeOptions[0].color = ["#5FB878","#1E9FFF"];
                    func.drawDataview(0, tradeChartsObjArr, tradeChartsContainers, tradeOptions);
                });

                func.switchCharts("LAY-index-dataview-trade", tradeChartsObjArr, tradeChartsContainers, tradeOptions);
            }
        },
        preOrder.render = function (e) { /* 建造实例 */
            return new implement(e);
        };

    let func = {
        /**
         * 绘制报表
         * @param index 下标
         * @param chartsObjArr 报表实例数组
         * @param chartsContainers 报表容器实例数组
         * @param options 报表参数
         */
        drawDataview: function (index, chartsObjArr, chartsContainers, options) {
            if (chartsObjArr[index] != null && chartsObjArr[index] !== void 0) {
                chartsObjArr[index].dispose();
                /* 销毁实例 */
            }
            let existInstance = echarts.getInstanceByDom(chartsContainers[index]);
            if (existInstance) { // 解决实例还存在
                existInstance.dispose();
            }
            chartsObjArr[index] = echarts.init(chartsContainers[index]);
            chartsObjArr[index].setOption(options[index]);
        },

        /**
         *
         * @param filter lay-filter 监听参数
         * @param chartsObjArr 报表实例数组
         * @param chartsContainers 报表容器实例数组
         * @param options 报表参数
         */
        switchCharts: function (filter, chartsObjArr, chartsContainers, options) {
            let index = 0;
            carousel.on(`change(${filter})`, function (e) { /* 轮播监听 */
                func.drawDataview(index = e.index, chartsObjArr, chartsContainers, options);
            })
                , nav.on("shrink(shrink)", function (e) { /* 自定义监听 */
                setTimeout(function () {
                    func.chartsResize(chartsContainers[index], chartsObjArr[index]);
                }, 400);
            });
            /* 解决echarts——bug echarts图表默认隐藏的时候，是不渲染的，等再次有数据的时候就会渲染，但这时候渲染的默认宽度是100px。*/
            window.onresize = () => {
                func.chartsResize(chartsContainers[index], chartsObjArr[index]);
            }
        },
        /**
         * 报表重置大小
         * @param elemContainer 报表容器选择器
         * @param chartsObj 报表对象
         */
        chartsResize: function (elemContainer, chartsObj) {
            let resize = {
                width: $(elemContainer).width(),
                height: $(elemContainer).height()
            };
            chartsObj.resize(resize);
        }

    };
    exports(modelName, preOrder);
});

