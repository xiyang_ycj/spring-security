package com.xiyang.web;

import com.xiyang.common.entity.Permission;
import com.xiyang.common.entity.Role;
import com.xiyang.common.repository.PermissionRepository;
import com.xiyang.common.repository.RoleRepository;
import com.xiyang.common.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * @author xiyang.ycj
 * @since Aug 09, 2019 15:40:49 PM
 */
public class InsertData extends SecurityWebApplicationTests{

    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;


    private Permission buyer(){
        Permission configure = new Permission();
        configure.setCode("dev_buyer");
        configure.setDescription("1管理");
        configure.setPermissionName("1管理");
        configure.setType(2);
        configure.setUrl("/dev/buyer");
        configure.setIcon("layui-icon layui-icon-extend layui-icon-buyer");
        configure.setSort(1L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission goods(){
        Permission configure = new Permission();
        configure.setCode("dev_goods");
        configure.setDescription("2管理");
        configure.setPermissionName("2管理");
        configure.setType(2);
        configure.setUrl("/dev/goods");
        configure.setIcon("layui-icon layui-icon-extend layui-icon-goods");
        configure.setSort(2L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission store(){
        Permission configure = new Permission();
        configure.setCode("dev_store");
        configure.setDescription("3管理");
        configure.setPermissionName("3管理");
        configure.setType(2);
        configure.setUrl("/dev/store");
        configure.setIcon("layui-icon layui-icon-extend layui-icon-store");
        configure.setSort(3L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission order(){
        Permission configure = new Permission();
        configure.setCode("dev_order");
        configure.setDescription("4管理");
        configure.setPermissionName("4管理");
        configure.setType(2);
        configure.setUrl("/dev/order");
        configure.setIcon("layui-icon layui-icon-extend layui-icon-order");
        configure.setSort(4L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission log(){
        Permission configure = new Permission();
        configure.setCode("log");
        configure.setDescription("5管理");
        configure.setPermissionName("5管理");
        configure.setType(3);
        configure.setUrl("log");
        configure.setIcon("layui-icon layui-icon-log");
        configure.setSort(5L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission callback(Integer parentId){
        Permission configure = new Permission();
        configure.setCode("dev_callback");
        configure.setDescription("5管理-1");
        configure.setPermissionName("5管理-1");
        configure.setType(1);
        configure.setUrl("/dev/callback");
        configure.setIcon("layui-icon layui-icon-log");
        configure.setParentId(parentId);
        configure.setSort(6L);
        return permissionRepository.saveAndFlush(configure);
    }


    private Permission 设置(){
        Permission configure = new Permission();
        configure.setCode("configure");
        configure.setDescription("设置");
        configure.setPermissionName("设置");
        configure.setType(3);
        configure.setUrl("/configure");
        configure.setIcon("layui-icon layui-icon-set");
        configure.setSort(7L);
        return permissionRepository.saveAndFlush(configure);
    }

    private Permission 系统设置(Integer parentId){
        Permission system_configure = new Permission();
        system_configure.setCode("system_configure");
        system_configure.setDescription("系统设置");
        system_configure.setPermissionName("系统设置");
        system_configure.setType(0);
        system_configure.setUrl("system/configure");
        system_configure.setParentId(parentId);
        system_configure.setSort(8L);
        return permissionRepository.saveAndFlush(system_configure);
    }

    private Permission 我的设置(Integer parentId){
        Permission user_configure = new Permission();
        user_configure.setCode("user_configure");
        user_configure.setDescription("我的设置");
        user_configure.setPermissionName("我的设置");
        user_configure.setType(0);
        user_configure.setUrl("user/configure");
        user_configure.setParentId(parentId);
        user_configure.setSort(9L);
        return permissionRepository.saveAndFlush(user_configure);
    }

    private Permission 下单配置(Integer parentId){
        Permission order_configure = new Permission();
        order_configure.setCode("order_configure");
        order_configure.setDescription("1配置");
        order_configure.setPermissionName("1配置");
        order_configure.setType(0);
        order_configure.setUrl("user/order/configure");
        order_configure.setParentId(parentId);
        order_configure.setSort(10L);
        return permissionRepository.saveAndFlush(order_configure);
    }


    private Permission 基本资料(Integer parentId){
        Permission user_basicData = new Permission();
        user_basicData.setCode("user_basicData");
        user_basicData.setDescription("基本资料");
        user_basicData.setPermissionName("基本资料");
        user_basicData.setType(0);
        user_basicData.setUrl("user/basicData");
        user_basicData.setParentId(parentId);
        user_basicData.setSort(11L);
        return permissionRepository.saveAndFlush(user_basicData);
    }

    private Permission 修改密码(Integer parentId){
        Permission user_changePassword = new Permission();
        user_changePassword.setCode("user_changePassword");
        user_changePassword.setDescription("修改密码");
        user_changePassword.setPermissionName("修改密码");
        user_changePassword.setType(0);
        user_changePassword.setUrl("user/changePassword");
        user_changePassword.setParentId(parentId);
        user_changePassword.setSort(12L);
        return permissionRepository.saveAndFlush(user_changePassword);
    }

    private Permission 码库管理(){
        Permission code_stock_manage = new Permission();
        code_stock_manage.setCode("code_stock_manage");
        code_stock_manage.setDescription("6管理");
        code_stock_manage.setPermissionName("6管理");
        code_stock_manage.setType(3);
        code_stock_manage.setUrl("/codeStock");
        code_stock_manage.setIcon("layui-icon layui-icon-template-1");
        code_stock_manage.setSort(13L);
        return permissionRepository.saveAndFlush(code_stock_manage);
    }

    private Permission 预下单(Integer parentId){
        Permission user_changePassword = new Permission();
        user_changePassword.setCode("code_stock_pre_order");
        user_changePassword.setDescription("6管理-1");
        user_changePassword.setPermissionName("6管理-1");
        user_changePassword.setType(1);
        user_changePassword.setUrl("/codeStock/preOrder");
        user_changePassword.setParentId(parentId);
        user_changePassword.setSort(14L);
        return permissionRepository.saveAndFlush(user_changePassword);
    }

    private Permission 码库信息(Integer parentId){
        Permission user_changePassword = new Permission();
        user_changePassword.setCode("code_stock_info");
        user_changePassword.setDescription("6管理-2");
        user_changePassword.setPermissionName("6管理-2");
        user_changePassword.setType(1);
        user_changePassword.setUrl("/codeStock/index");
        user_changePassword.setParentId(parentId);
        user_changePassword.setSort(15L);
        return permissionRepository.saveAndFlush(user_changePassword);
    }


    private Permission 平台管理(){
        Permission admin_manage = new Permission();
        admin_manage.setCode("admin_manage");
        admin_manage.setDescription("平台管理");
        admin_manage.setPermissionName("平台账户管理");
        admin_manage.setType(2);
        admin_manage.setUrl("/admin/index");
        admin_manage.setSort(16L);
        return permissionRepository.saveAndFlush(admin_manage);
    }

    private Permission 注册账号(){
        Permission admin_register = new Permission();
        admin_register.setCode("admin_register");
        admin_register.setDescription("注册账号");
        admin_register.setPermissionName("账号注册");
        admin_register.setType(2);
        admin_register.setUrl("/admin/register");
        admin_register.setSort(17L);
        return permissionRepository.saveAndFlush(admin_register);
    }

    private Permission 角色注册(){
        Permission admin_addUser = new Permission();
        admin_addUser.setCode("admin_role");
        admin_addUser.setDescription("角色注册");
        admin_addUser.setPermissionName("角色注册");
        admin_addUser.setType(2);
        admin_addUser.setUrl("/admin/role");
        admin_addUser.setSort(18L);
        return permissionRepository.saveAndFlush(admin_addUser);
    }

    @Test
    public void insertData(){

        /*-----------------basic------------------------*/
        Permission buyer = buyer();
        Permission goods = goods();
        Permission store = store();
        Permission order = order();

        Permission log = log();
        Permission callback = callback(log.getId());


        //Permission 拼多多店铺管理 = 拼多多店铺管理();
        Permission 码库管理 = 码库管理();
            Permission 预下单 = 预下单(码库管理.getId());
            Permission 码库信息 = 码库信息(码库管理.getId());

        Permission 设置 = 设置();
            Permission 系统设置 = 系统设置(设置.getId());
                Permission 下单配置 = 下单配置(系统设置.getId());
            Permission 我的设置 = 我的设置(设置.getId());
                Permission 基本资料 = 基本资料(我的设置.getId());
                Permission 修改密码 = 修改密码(我的设置.getId());

        Permission 平台管理 = 平台管理();
        Permission 注册账号 = 注册账号();

        Permission 角色注册 = 角色注册();
        /* 下单配置 */


        Set<Permission> permissions = new HashSet<>();
            permissions.add(buyer);
            permissions.add(goods);
            permissions.add(store);
            permissions.add(order);
            permissions.add(log);
                permissions.add(callback);
//            permissions.add(码库管理);
//                permissions.add(预下单);
//                permissions.add(码库信息);
            permissions.add(设置);
                permissions.add(系统设置);
                    permissions.add(下单配置);
                permissions.add(我的设置);
                    permissions.add(基本资料);
                    permissions.add(修改密码);

//        Role role = new Role();
//        role.setRoleCode("");
//        role.setRoleName("");
//        role.setPermissions(permissions);
//        Role pdd = roleRepository.saveAndFlush(role);
        /*-----------------------------------------*/

        /*------------------admin-----------------*/

        Set<Permission> 管理员 = new HashSet<>();
            管理员.add(平台管理);
            管理员.add(注册账号);
            管理员.add(角色注册);
        Role role1 = new Role();
        role1.setRoleName("平台管理员");
        role1.setRoleCode("ROLE_ADMIN");
        role1.setPermissions(管理员);
        Role admin = roleRepository.saveAndFlush(role1);
//
//        BCryptPasswordEncoder bc=new BCryptPasswordEncoder(4);
//        Set<Role> roles = new HashSet<>();
//        roles.add(admin);
//        User user= new User();
//        user.setRealname("超级管理员").setUsername("admin").setPassword(bc.encode("admin")).setSex(1).setRoles(roles);
//        userRepository.saveAndFlush(user);

    }


}
