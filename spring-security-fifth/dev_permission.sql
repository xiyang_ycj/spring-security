/*
Navicat MySQL Data Transfer

Source Server         : ls
Source Server Version : 50725
Source Host           : ds.aliyuncs.com:3306
Source Database       : liansen_bank

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-12-09 17:51:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dev_permission
-- ----------------------------
DROP TABLE IF EXISTS `dev_permission`;
CREATE TABLE `dev_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(120) DEFAULT NULL COMMENT '权限描述',
  `permission_name` varchar(120) DEFAULT NULL COMMENT '权限名称',
  `url` varchar(120) DEFAULT NULL COMMENT '权限地址',
  `code` varchar(120) DEFAULT NULL COMMENT '权限代码',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是菜单,默认不是 0:路径，1，子菜单，2菜单',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id用于多级按钮',
  `icon` varchar(120) DEFAULT NULL COMMENT '按钮图标',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
