package com.xiyang.security.config.security.authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * 权限决策 AccessDecisionManager
 * 根据URL资源权限和用户角色权限
 *
 * {@link AccessDecisionManager} 鉴权决策管理器 <br/>
 * 被鉴权决策管理器 被{@link org.springframework.security.access.intercept.AbstractSecurityInterceptor} 调用进行鉴权 <br/>
 * 框架默认实现是 {@link org.springframework.security.access.vote.UnanimousBased}
 *
 * @author xiyang.ycj
 * @since Jun 26, 2019 10:48:43 AM
 */
@Service
public class CustomAccessDecisionManager implements AccessDecisionManager {

    private static final Logger log = LoggerFactory.getLogger(CustomAccessDecisionManager.class);

    /**
     * 权限鉴定
     *
     * @param authentication   from SecurityContextHolder.getContext() => userDetails.getAuthorities()
     * @param object           就是FilterInvocation对象，可以得到request等web资源。
     * @param configAttributes from MetaDataSource.getAttributes()，已经被框架做了非空判断
     * @throws AccessDeniedException   如果由于身份验证不具有所需的权限或ACL特权而拒绝访问
     * @throws InsufficientAuthenticationException 如果由于身份验证没有提供足够的信任级别而拒绝访问
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {

        log.info("****************************************权限鉴定********************************************");
        /*FilterInvocation filterInvocation = (FilterInvocation) object; // object 是一个URL
        log.info("[当前路径[{}]需要的资源权限]：{}",filterInvocation.getRequestUrl(),configAttributes);*/
        log.info("[登录用户[{}]权限]：{}",authentication.getName(),authentication.getAuthorities());

        if(configAttributes == null){
            return;
        }

        for (ConfigAttribute configAttribute : configAttributes) {
            /* 资源的权限 */
            String attribute = configAttribute.getAttribute();
            /* 用户的权限 */
            for (GrantedAuthority authority : authentication.getAuthorities()) { // 当前用户的权限
                if(authority.getAuthority().trim().equals("ROLE_ANONYMOUS"))return;
                log.info("[资源角色==用户角色] ？ {} == {}", attribute.trim(), authority.getAuthority().trim());
                if (attribute.trim().equals(authority.getAuthority().trim())) {
                    log.info("[鉴权决策管理器]：登录用户[{}]权限匹配",authentication.getName());
                    return;
                }
            }
        }
        log.info("[鉴权决策管理器]：登录用户[{}]权限不足",authentication.getName());
        throw new AccessDeniedException("权限不足");
    }

    /**
     * AbstractSecurityInterceptor 调用，遍历ConfigAttribute集合，筛选出不支持的attribute
     *
     * @param attribute a configuration attribute that has been configured against the
     *                  <code>AbstractSecurityInterceptor</code>
     * @return true if this <code>AccessDecisionManager</code> can support the passed
     * configuration attribute
     */
    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    /**
     * AbstractSecurityInterceptor 调用,验证AccessDecisionManager是否支持这个安全对象的类型。
     * supports(Class)方法被安全拦截器实现调用，
     * 包含安全拦截器将显示的AccessDecisionManager支持安全对象的类型。
     *
     * @param clazz the class that is being queried
     * @return <code>true</code> if the implementation can process the indicated class
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
