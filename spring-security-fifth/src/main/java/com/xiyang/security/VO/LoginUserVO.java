package com.xiyang.security.VO;

/**
 * @author xiyang.ycj
 * @since Jul 08, 2019 17:58:02 PM
 */
public class LoginUserVO {
    private String uuid;
    private String code;
    private String username;
    private String password;

    public LoginUserVO() {
    }

    public LoginUserVO(String uuid, String code, String username, String password) {
        this.uuid = uuid;
        this.code = code;
        this.username = username;
        this.password = password;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginUserVO{" +
                "uuid='" + uuid + '\'' +
                ", code='" + code + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
