package com.xiyang.security.repository;

import com.xiyang.security.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xiyang.ycj
 * @since Jul 05, 2019 14:33:51 PM
 */
@Repository
public interface PermissionRepository extends JpaRepository<Permission,Integer> {
}
