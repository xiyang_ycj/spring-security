/*
Navicat MySQL Data Transfer

Source Server         : ls
Source Server Version : 50725
Source Host           : rs.com:3306
Source Database       : devchannel

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-11-18 18:00:19
*/
                     
-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(120) DEFAULT NULL COMMENT '权限代码',
  `description` varchar(120) DEFAULT NULL COMMENT '权限描述',
  `icon` varchar(120) DEFAULT NULL COMMENT '按钮图标',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id用于多级按钮',
  `permission_name` varchar(120) DEFAULT NULL COMMENT '权限名称',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是菜单,默认不是 0:路径，1，子菜单，2菜单',
  `url` varchar(120) DEFAULT NULL COMMENT '权限地址',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for dev_role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(120) DEFAULT NULL COMMENT '角色中文名用于显示',
  `rolename` varchar(120) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `FK9tx50w25el74x981cuhd580py` (`role_id`),
  CONSTRAINT `FK9tx50w25el74x981cuhd580py` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  CONSTRAINT `FKtdg2ky40s637kb0gfxcudaj74` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(250) DEFAULT NULL COMMENT '用户密码',
  `realname` varchar(120) DEFAULT NULL COMMENT '用户真名',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户性别 0:未知，1:男，2:女',
  `username` varchar(120) DEFAULT NULL COMMENT '用户账号',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKri1jpdho1da9roqq58l5jfg6s` (`role_id`),
  CONSTRAINT `FKkisb73h8ujsjxuvpivyy2qls6` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FKri1jpdho1da9roqq58l5jfg6s` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

