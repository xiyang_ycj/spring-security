package com.xiyang.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring security
 * @author xiyang.ycj
 * @since May 30, 2019 22:44:07 PM
 */
@SpringBootApplication()
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

}
