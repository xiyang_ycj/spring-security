package com.xiyang.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author xiyang.ycj
 * @since Jun 25, 2019 02:11:49 AM
 */
@Entity
@Table(name = "permission",catalog = "xiyang_operater")
public class Permission implements Serializable {


    private static final long serialVersionUID = 2987536760299464485L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "permission_id")
    private Integer permissionId;
    @Column(name = "permission_name",columnDefinition = "varchar(120) comment '权限名称' ")
    private String  permissionName;
    @Column(name = "description",columnDefinition = "varchar(120) comment '权限描述' ")
    private String  description;
    @Column(name = "url",columnDefinition = "varchar(120) comment '权限地址' ")
    private String  url;

    public Permission() {
    }

    public Permission(Integer permissionId,String permissionName, String description, String url) {
        this.permissionId = permissionId;
        this.permissionName = permissionName;
        this.description = description;
        this.url = url;
    }

    //关系两边都作为主控
    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    @JoinTable(name = "role_permission", joinColumns = { @JoinColumn(name = "permission_id") }, inverseJoinColumns = {
            @JoinColumn(name = "role_id") })//joinColumns写的都是本表在中间表的外键名称， inverseJoinColumns写的是另一个表在中间表的外键名称。
    @JsonIgnore
    private Set<Role> roles;

    @Transient
    @JsonIgnore
    private Set<String> roleNames;

    /* @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id",nullable = false) // 外键列是否可以为空。
    private Role role; // 角色对应的用户实体*/

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<String> getRoleNames() {
        if(roleNames==null){
            roleNames=new HashSet<>();
            for (Role role : roles) {
                this.roleNames.add(role.getRolename());
            }
        }
        return roleNames;
    }

    public void setRoleNames(Set<String> roleNames) {
        this.roleNames = roleNames;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "permissionId=" + permissionId +
                ", permissionName='" + permissionName + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", roleNames=" + roleNames +
                '}';
    }
}
