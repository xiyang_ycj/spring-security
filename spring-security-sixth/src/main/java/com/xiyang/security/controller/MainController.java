package com.xiyang.security.controller;

import com.xiyang.security.VO.ImgVO;
import com.xiyang.security.common.BaseResult;
import com.xiyang.security.common.ReturnCode;
import com.xiyang.security.utils.RedisUtil;
import com.xiyang.security.utils.UUIDUtil;
import com.xiyang.security.utils.VerifyCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author xiyang.ycj
 * @since Jun 22, 2019 00:19:37 AM
 */
@RestController
public class MainController {


    private final RedisUtil redisUtil;
    private final long time;

    public MainController(@Autowired RedisUtil redisUtil, @Value("${loginCode.expiration}") long time) {
        this.redisUtil = redisUtil;
        this.time = time;
    }



    /**
     * 如果自动跳转到这个页面，说明用户未登录，返回相应的提示即可
     * <p>
     * 如果要支持表单登录，可以在这个方法中判断请求的类型，进而决定返回JSON还是HTML页面
     *
     * @return
     */
    @GetMapping("/login_page")
    public Object loginPage() {
        return new BaseResult(ReturnCode.FAILED.getCode(), "尚未登录，请登录!");
    }


    @GetMapping(value = "/123")
    public Object login(){
        return "hello world";
    }



    @GetMapping("/getCode")
    public Object getCode(HttpServletRequest request) {

        /* 生成验证码字符串 */
        String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
        String uuid = UUIDUtil.GeneratorUUIDOfSimple();

        /* 保存验证码到redis */
        redisUtil.setExpire(uuid,verifyCode,time);

        int w = 111,h = 36;

        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            VerifyCodeUtil.outputImage(w, h, stream, verifyCode);
            return new BaseResult(ReturnCode.SUCCESS.getCode(), ReturnCode.SUCCESS.getMessage(),new ImgVO("data:image/gif;base64,"+ Base64Utils.encodeToString(stream.toByteArray()),uuid));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
