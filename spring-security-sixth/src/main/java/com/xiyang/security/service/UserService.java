package com.xiyang.security.service;

import com.xiyang.security.entity.User;
import com.xiyang.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xiyang.ycj
 * @since Jun 24, 2019 23:40:25 PM
 */
@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserByUsername(String username){
        return userRepository.findUserByUsername(username);
    }

    public User saveAndFlush(User user){
        return userRepository.saveAndFlush(user);
    }
}
