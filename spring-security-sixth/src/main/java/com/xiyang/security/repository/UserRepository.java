package com.xiyang.security.repository;

import com.xiyang.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xiyang.ycj
 * @since Jun 22, 2019 01:28:22 AM
 */
@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User findUserByUsername(String username);
}
